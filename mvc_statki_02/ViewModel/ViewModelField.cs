﻿namespace mvc_statki_02.ViewModel
{
    public enum State
    {
        Empty,
        Ship,
        Miss,
        Shoted
    }
   public class ViewModelField
    {
        public int Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public State State { get; set; }
        public int PlayerId { get; set; }
    }
}
