﻿using System.Web.Mvc;
using System.Web.Routing;
using AutoMapper;
using mvc_statki_02.Models;
using mvc_statki_02.ViewModel;

namespace mvc_statki_02
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            Mapper.CreateMap<Player, PlayerViewModel>();
            Mapper.CreateMap<PlayerViewModel, Player>();
            Mapper.CreateMap<Field, ViewModelField>();
            Mapper.CreateMap<ViewModelField, Field>();
        }
    }
}
