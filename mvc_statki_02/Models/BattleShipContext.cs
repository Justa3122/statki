﻿using System.Data.Entity;

namespace mvc_statki_02.Models
{
    class ShipGameContext:DbContext
    {
        public ShipGameContext():base("BattleShipBase")
        {
            
        }
        public DbSet<Player> Players { get; set; }
        public DbSet<Field> Fields { get; set; }
    }
}
