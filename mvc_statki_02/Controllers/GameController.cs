﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using mvc_statki_02.Models;
using mvc_statki_02.ViewModel;
using State = mvc_statki_02.Models.State;

namespace mvc_statki_02.Controllers
{
    public class GameController : Controller
    {
        private int _x = 6;
        private int _y = 6;
        private IList<PlayerViewModel> Players { get; set; }
        public ActionResult ShowMap()
        {
            CreateMap();
            var viewmodel = new List<MapViewModel>();
            using (var db = new ShipGameContext())
            {
                foreach (var player in Players)
                {
                    var fields = db.Fields.Where(x => x.PlayerId == player.Id).ToList();
                    var fieldsVieModel = Mapper.Map<IList<Field>, IList<ViewModelField>>(fields);
                    viewmodel.Add(new MapViewModel { LengthMap = _x, HighMap = _y, NamePlayer = player.Name, IdPlayer = player.Id, Fields = fieldsVieModel });
                }
                viewmodel.First().IdOpponent = viewmodel.Last().IdPlayer;
                viewmodel.Last().IdOpponent = viewmodel.First().IdPlayer;
                viewmodel.First().IsGo = true;

                return View(viewmodel);
            }
        }


        //public ActionResult ShowMap(MapViewModel model)
        //{
        //    var viewmodel1 = new List<MapViewModel>();
        //    bool isHit = false;
        //    var state =
        //        db.Fields.FirstOrDefault(x => x.PlayerId == model.IdOpponent && x.X == model.ShotX && x.Y == model.ShotY);
        //}


           



        [HttpPost]
        public ActionResult ShowMap(MapViewModel model)
        {
            using (var db = new ShipGameContext())
            {
                var viewmodel1 = new List<MapViewModel>();
                bool isHit = false;
                var state =
                    db.Fields.FirstOrDefault(x => x.PlayerId == model.IdOpponent && x.X == model.ShotX && x.Y == model.ShotY);
                if (state.State == State.Ship)
                {
                    db.Fields
                    .Single(x => x.PlayerId == model.IdOpponent && x.X == model.ShotX && x.Y == model.ShotY)
                    .State=State.Shoted;
                    isHit = true;
                }
                if (state.State == State.Empty)
                {
                    db.Fields
                    .Single(x => x.PlayerId == model.IdOpponent && x.X == model.ShotX && x.Y == model.ShotY)
                    .State = State.Miss;
                }

                db.SaveChanges();

                var players =
                    db.Players.ToList();

                foreach (var player in players)
                {
                    var fields = db.Fields.Where(x => x.PlayerId == player.Id).ToList();
                    var fieldsVieModel = Mapper.Map<IList<Field>, IList<ViewModelField>>(fields);
                    viewmodel1.Add(new MapViewModel { LengthMap = _x, HighMap = _y, IdOpponent = players.First(x => x.Id != player.Id).Id, NamePlayer = player.Name, IdPlayer = player.Id, Fields = fieldsVieModel });
                }

                foreach (var map in viewmodel1)
                {
                    if (map.Fields.FirstOrDefault(x=>x.State==ViewModel.State.Ship) == null)
                    {
                        var idWinner = map.IdOpponent;
                        viewmodel1.First(x => x.IdPlayer == idWinner).IsWinner = true;
                        break;
                    }
                }
            

                if (isHit)
                    viewmodel1.First(x => x.IdPlayer == model.IdPlayer).IsGo = true;
                else
                {
                    viewmodel1.First(x => x.IdPlayer == model.IdOpponent).IsGo = true;
                }
                ModelState.Clear();
                return View(viewmodel1);
            }
        }
        private void CreateMap()
        {
            using (var db = new ShipGameContext())
            {
                db.Database.ExecuteSqlCommand("TRUNCATE TABLE [Fields]");
                var rnd = new Random(DateTime.Now.Millisecond);
                var count = db.Players.Count();
                var players = db.Players.OrderBy(x => x.Id).Skip(count - 2).ToList();
                Players = Mapper.Map<IList<Player>, IList<PlayerViewModel>>(players);
                for (int i = 0; i < 2; i++)
                {
                    var list = new List<Field>();

                    for (int j = 0; j < _x; j++)
                    {
                        for (int k = 0; k < _y; k++)
                        {
                            list.Add(new Field { X = j, Y = k, State = State.Empty, PlayerId = Players[i].Id });
                        }
                    }

                    int stateCount = 0;
                    while (stateCount < 6)
                    {
                      stateCount++;
                    }
                    db.Fields.AddRange(list);
                    db.SaveChanges();
                }
            }
        }

       
    }
}